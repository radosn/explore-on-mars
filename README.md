## Rovers on Mars

#### Install project

Clone repository
```
git clone git@bitbucket.org:radosn/explore-on-mars.git
```

Install Symfony
```
cd explore-on-mars/
composer install
```

Run the project
```
php -S localhost:8080
http://localhost:8080/explore
```

LIVE Example
```
http://rover.rktaskmanager.com/explore
```

Info

- There are 3 available TXT files with parameters for Rovers in the root of the project

