<?php

namespace App\Tests\Controller;

use App\Model\Direction;
use App\Model\Rover;
use PHPUnit\Runner\Exception;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExplorerControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $client->request('GET', 'explore');

        $readParams = [
            '5 5',
            '1 3 N',
            'MMRMM'
        ];
        for($i = 1; $i < count($readParams); $i++) {
            $row = explode(' ', $readParams[$i]);
            $direction = new Direction();
            $direction->setDirection($row[2]);
            $rover = new Rover($row[0], $row[1], $direction);

            $i++;
            $commands = $readParams[$i];
            foreach (str_split($commands) as $command) {
                try {
                    $rover->doAction($command);
                } catch (\Exception $e) {
                    throw new Exception($e->getMessage());
                }
            }

            $this->assertEquals(3, $rover->getX());
            $this->assertEquals(5, $rover->getY());
            $this->assertEquals('E', $rover->getDirection()->getDirectionString());
        }

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
