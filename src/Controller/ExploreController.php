<?php

namespace App\Controller;

use App\Service\ProcessRoverDataService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

class ExploreController extends AbstractController
{
    /**
     * @Route("/explore", name="explore")
     * @param Request $request
     * @param ProcessRoverDataService $processRoverDataService
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request, ProcessRoverDataService $processRoverDataService)
    {
        $defaultData = [
            'message' => 'Upload Rover(s) parameters',
        ];

        $form = $this->createFormBuilder($defaultData)
            ->add('parameters', FileType::class, [
                'label' => false,
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'text/plain',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid TXT document',
                    ])
                ],
            ])
            ->add('upload', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        $rovers = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $rovers = $processRoverDataService->run($data);
        }

        return $this->render('explore/index.html.twig', [
            'form' => $form->createView(),
            'rovers' => $rovers,
        ]);
    }
}
