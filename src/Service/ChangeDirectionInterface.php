<?php

namespace App\Service;

interface ChangeDirectionInterface
{
    public function rotate90DegreesRight();
    public function rotate90DegreesLeft();
    public function getDirection();
    public function setDirection($direction);
}