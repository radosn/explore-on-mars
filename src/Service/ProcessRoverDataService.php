<?php

namespace App\Service;

use App\Model\Direction;
use App\Model\Rover;
use Exception;

class ProcessRoverDataService
{
    public function run($data)
    {
        $readParams = file_get_contents($data['parameters']->getPathname());
        $readParams = explode(PHP_EOL, $readParams);

        $borderX = $this->getBorderX($readParams);
        $borderY = $this->getBorderY($readParams);

        $this->isValidInput($readParams);

        $rovers = [];
        for($i = 1; $i < count($readParams); $i++) {
            $row = explode(' ', $readParams[$i]);

            $this->isValidRoverPosition($row);

            $direction = new Direction();
            $direction->setDirection($row[2]);
            $rover = new Rover((int) $row[0], (int) $row[1], $direction);

            $i++;
            $commands = $readParams[$i];
            foreach (str_split($commands) as $command) {
                try {
                    $rover->doAction($command);
                    if ($rover->getX() > $borderX
                        || $rover->getX() < 0
                        || $rover->getY() > $borderY
                        || $rover->getY() < 0
                    ) {
                        throw new Exception('Rover is out of borders!');
                    }
                } catch (Exception $exception) {
                    throw new Exception($exception->getMessage());
                }
            }

            $rovers[] = $rover;
        }

        return $rovers;
    }

    /**
     * @param $input
     * @throws Exception
     */
    public function isValidInput($input)
    {
        if (count($input) === 1) {
            throw new Exception('There is no rovers.');
        }
    }

    /**
     * @param $input
     * @return int
     * @throws Exception
     */
    public function getBorderX($input)
    {
        if (isset($input[0]) && isset($input[0][0])) {
            return (int) $input[0][0];
        }

        throw new Exception('Not a valid parameter for border coordinate.');
    }

    /**
     * @param $input
     * @return int
     * @throws Exception
     */
    public function getBorderY($input)
    {
        if (isset($input[0]) && isset($input[0][2])) {
            return (int) $input[0][2];
        }

        throw new Exception('Not a valid parameter for border coordinate.');
    }

    /**
     * @param $input
     * @throws Exception
     */
    public function isValidRoverPosition($input)
    {
        if (!isset($input[0]) || !isset($input[1]) || !isset($input[2])) {
            throw new Exception('Invalid position.');
        }
    }
}