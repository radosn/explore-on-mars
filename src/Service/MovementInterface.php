<?php

namespace App\Service;

interface MovementInterface
{
    public function makeMove();
}