<?php

namespace App\Model;

use App\Service\ChangeDirectionInterface;

class Direction implements ChangeDirectionInterface
{
    public const N = 1;
    public const E = 2;
    public const S = 3;
    public const W = 4;

    private $value;

    public function rotate90DegreesRight()
    {
        $this->value++;
        if ($this->value > self::W) {
            $this->value = self::N;
        }
    }

    public function rotate90DegreesLeft()
    {
        $this->value--;
        if ($this->value < self::N) {
            $this->value = self::W;
        }
    }

    public function getDirection()
    {
        return $this->value;
    }

    public function setDirection($direction)
    {
        switch ($direction) {
            case 'N' :
                $this->value = Direction::N;
                break;
            case 'S' :
                $this->value = Direction::S;
                break;
            case 'E' :
                $this->value = Direction::E;
                break;
            case 'W' :
                $this->value = Direction::W;
                break;
            default :
                throw new \Exception('Not a valid direction passed!');
        }
    }

    public function getDirectionString()
    {
        switch ($this->value) {
            case Direction::N :
                return 'N';
            case Direction::S :
                return 'S';
            case Direction::E :
                return 'E';
            case Direction::W :
                return 'W';
            default :
                throw new \Exception('Not a valid direction value!');
        }
    }
}