<?php

namespace App\Model;

use App\Service\MovementInterface;
use App\Service\ChangeDirectionInterface;

class Rover implements MovementInterface
{
    private $x;
    private $y;
    /**
     * @var ChangeDirectionInterface $direction
     */
    private $direction;

    /**
     * Rover constructor.
     * @param $x
     * @param $y
     * @param ChangeDirectionInterface $direction
     */
    public function __construct(int $x, int $y, ChangeDirectionInterface $direction)
    {
        $this->x = $x;
        $this->y = $y;
        $this->direction = $direction;
    }

    /**
     * @param $command
     * @throws \Exception
     */
    public function doAction($command)
    {
        switch ($command) {
            case 'L' :
                $this->direction->rotate90DegreesLeft();
                break;
            case 'R' :
                $this->direction->rotate90DegreesRight();
                break;
            case 'M' :
                $this->makeMove();
                break;
            default :
                throw new \Exception('Command not recognized.');
        }
    }

    public function makeMove()
    {
        switch ($this->direction->getDirection()) {
            case Direction::N :
                $this->y++;
                break;
            case Direction::S :
                $this->y--;
                break;
            case Direction::E :
                $this->x++;
                break;
            case Direction::W :
                $this->x--;
                break;
            default :
                throw new \Exception('Wrong direction passed!');
        }
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     */
    public function setX($x): void
    {
        $this->x = $x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     */
    public function setY($y): void
    {
        $this->y = $y;
    }

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param mixed $direction
     */
    public function setDirection($direction): void
    {
        $this->direction = $direction;
    }
}